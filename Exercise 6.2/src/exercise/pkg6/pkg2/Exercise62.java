package exercise.pkg6.pkg2;

import java.util.StringTokenizer;

public boolean setName(String nm) {
        String token;
        if (nm != null && nm.length() > 0) {
            name = nm;
            StringTokenizer strTok = new StringTokenizer(name, ", ");
            while(strTok.hasMoreTokens()) {
                token = strTok.nextToken();
                System.out.println(token);
            }
            return true;
        }
        return false;
    }
