package exercise.pkg10.pkg2;

import java.math.*;
import java.io.*;

public class Exercise102 {
    public static void main(String[] args) {
        double answer;
        try {
            answer = Quadratic.root(1, 2, 3, true);
            System.out.println("Root: " + answer);
        }
        catch(ImaginaryRootsException e){
            System.out.println("Error: " + e);
        }
        catch(NonQuadraticException e){
            System.out.println("Error: " + e);
        }
    }
}

class ImaginaryRootsException extends IOException{
    public ImaginaryRootsException() {
        super("Imaginary roots!");
    }
}

class  NonQuadraticException extends IOException{
    public NonQuadraticException() {
        super("Equation is non-quadratic!");
    }
}

class Quadratic {
    static double root(double a, double b, double c, boolean flag) 
                                    throws ImaginaryRootsException, NonQuadraticException{
        
        double root1, root2, temp;
        
        if(a == 0) {
            NonQuadraticException exp = new NonQuadraticException();
            throw(exp);
        }
        temp = Math.pow(b, 2) - (4*a*c);
        if(temp < 0) {
            ImaginaryRootsException exp = new ImaginaryRootsException();
            throw(exp);
        }
        root1 = (-b + Math.sqrt(temp))/(2*a);
        root2 = (-b - Math.sqrt(temp))/(2*a);
        
        if(flag) {
            return root1;
        }
        else {
            return root2;
        }
    }
}