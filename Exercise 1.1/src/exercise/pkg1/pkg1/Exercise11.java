
package exercise.pkg1.pkg1;

public class Exercise11 {
    public static void main(String[] args) {
        System.out.println("1. A java method is called what in C?\n"
                + "A function.\n");
        System.out.println("2. Java is compiled into:\n"
                + "Classes, with the .class extension.\n");
        System.out.println("3. The Java interpreter is called:\n"
                + "The Java Virtual Machine.\n");
    }
}
/*

1. A java method is called what in C?
A function.

2. Java is compiled into:
Classes, with the .class extension.

3. The Java interpreter is called:
The Java Virtual Machine.

BUILD SUCCESSFUL (total time: 0 seconds)

 */