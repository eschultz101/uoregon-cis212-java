package exercise7.pkg3;

interface Comparable {
    int compareTo(Object obj);
}

class Customer implements Comparable {
    private String name;
    private long acctNum;
    private SavingsAccount savAcct;
    
    public Customer(String tempStr, long tempInt, double tempDouble) {
        name = tempStr;
        acctNum = tempInt;
        savAcct = new SavingsAccount(tempDouble);
    }
    
    public int compareTo(Object obj) {
        int result = name.compareToIgnoreCase((String) obj);
        if (result != 0) {
            return result;
        } else if ((long) obj == acctNum) {
            if (obj.equals(savAcct)) {
                return 0;
            } else if( ((SavingsAccount)obj).getBalance() < savAcct.getBalance()) {
                    return -1;
                } else { return 1; }
            } else if ((long) obj < acctNum) {
                return -1;
            } else {
                return 1;
            }
        }
    
    public String getName() {
        return name;
    }
    
    public long getNum() {
        return acctNum;
    }
    
    public SavingsAccount getAcct() {
        return savAcct;
    }
    
    public void setName() {
        this.name = name;
    }
}

class SavingsAccount {

    private static double annualIntRate; // default 0.
    private double balance; // default 0.

    public SavingsAccount(double bal) {

        if (bal > 0) {
            balance = bal;
        }
    // else leave as 0

    } // end constructor

    public SavingsAccount() {
    }//default const.

    public double getBalance() {
        return balance;
    }

    public boolean transaction(double amount) {

        if (balance + amount >= 0) {

            balance += amount;
            return true;

        }// end if
        return false;

    } // end transaction

    public double addMonthlyInterest() {

        double interest;

        interest = balance * annualIntRate / 12.;
// should be rounded to nearest 100th
        balance += interest;
        return interest;

    } // end addMonthlyInterest

    public static boolean modifyIntRate(double newRate) {

        if (newRate >= 0) {

            annualIntRate = newRate;
            return true;

        }// end if

        return false;

    } // end modifyIntRate

    public static double getAnnualIntRate() {

        return annualIntRate;

    } // end getAnnualIntRate
} // end SavingsAccount