package exercise.pkg5.pkg4;

import java.util.Scanner;


public class Exercise54 {

    static Scanner scanner = new Scanner(System.in); 
    
    public static void main(String[] args) {
        
        Customer[] custAry;
        
        custAry = initCusts();
        
    }
    
    public static Customer[] initCusts() {
        
        String tempStr;
        long tempInt;
        double tempFloat;
        int numCusts;
        
        System.out.println("Number of customers you wish to enter: ");
        numCusts = scanner.nextInt();
        
        Customer[] custAry = new Customer[numCusts];
        
        for(int i = 0; i < numCusts; i++) {
            System.out.println("Enter name for customer " + (i + 1) + ": ");
            tempStr = scanner.next();
            System.out.println("Enter account number for customer " + (i + 1) + ": ");
            tempInt = scanner.nextInt();
            System.out.println("Enter balance for customer " + (i + 1) + ": ");
            tempFloat = scanner.nextFloat();
            
        custAry[i] = new Customer(tempStr, tempInt, tempFloat);
        
        }
        
        return custAry;
    }
}
class Customer {    
    
    private String name;
    private long acctNum;
    private double acctBal;
    
    public Customer (String tempStr, long tempInt, double tempFloat) {
        
        name = tempStr;
        acctNum = tempInt;
        acctBal = tempFloat;
        
    } // end constructor
} // end class