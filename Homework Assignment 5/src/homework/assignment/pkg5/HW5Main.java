
package homework.assignment.pkg5;

import java.util.Scanner;
import java.util.StringTokenizer;

public class HW5Main {
    static Scanner sc = new Scanner(System.in);
    
    public static void main(String [] args) {
        Pet[] petArray;
        String input;
        petArray = getTestPetArray();
        sorter(petArray);
        
        System.out.println("Contents of array: ");
        subLoop(petArray);
        
        if(numLarge(petArray) >= 0) {
            System.out.println("There is/are " + numLarge(petArray) + " large dog(s) in the array.");
        } else {
          System.out.println("There are no large dogs");
        }
        
        petArray = getPetArray();
        sorter(petArray);
        
        System.out.println("Contents of array: ");
        subLoop(petArray);
        
        if(numLarge(petArray) >= 0) {
            System.out.println("There is/are " + numLarge(petArray) + " large dog(s) in the array.");
        } else {
          System.out.println("There are no large dogs");
        }
        
   }
    
    public static Pet[] getPetArray() {
        Pet[] petArray;
        System.out.println("Enter number of pets: ");
        int numPets = sc.nextInt();
        sc.nextLine();
        if (numPets >= 1 && numPets <= 20) {
            petArray = new Pet[numPets];
            for (int i = 0; i < numPets; i++) {
                petArray[i] = parser(numPets);
            }
            return petArray;
        }
        return new Pet[0];
    }
    
    public static Pet parser(int numPets) {
            System.out.println("Enter pet info: ");
            String input = sc.nextLine();
        StringTokenizer st = new StringTokenizer(input, ",");
        if(st.countTokens() == 4) {
            return new Dog(st.nextToken(), Integer.parseInt(st.nextToken()), 
                    Integer.parseInt(st.nextToken()), st.nextToken().charAt(0));
        }
        else return new Cat(st.nextToken(), Integer.parseInt(st.nextToken()), 
                Boolean.getBoolean(st.nextToken()));
    }
    
    public static void sorter(Pet[] petArray) {
        Pet temp;
        for (int j = 0; j < petArray.length; j++) {
            for (int i = j + 1; i < petArray.length; i++) {
                if (petArray[i].name.compareToIgnoreCase(petArray[j].name) < 0) {
                    temp = petArray[j];
                    petArray[j] = petArray[i];
                    petArray[i] = temp;
                }
            }
        }
    }
    
    public static void subLoop(Pet[] petArray) {
        for(Pet pet:petArray) {
            System.out.println(pet.toString() + " converted age= " + 
                    pet.convertYears() + " life expectancy= " + pet.lifeExpectancy());
        }
    }
    
    public static int numLarge(Pet[] petArray) {
        int numLDogs =  0;
        boolean dogs = false;
        for (int i = 0; i < petArray.length; i++) {
            if (petArray[i] instanceof Dog) {
                dogs = true;
                if (Character.toUpperCase(((Dog)petArray[i]).size) == 'L') {
                    numLDogs++;
                }
            }
        }
        if(dogs) {
            return numLDogs;
        }
        return -1;
    }
    
    public static Pet [] getTestPetArray(){
	return new Pet[]{
		new Dog("Doby Doberman", 2006, 7, 'L'),
		new Dog("Wienie Dog", 2003, 3, 's'),
		new Cat("Calico Cat", 2006, true),
		new Dog("banjo dog", 2008, 2, 'l'),
		new Cat("FRAIDY CAT", 2010, false)
	};
} //end getTestPetArray
}
