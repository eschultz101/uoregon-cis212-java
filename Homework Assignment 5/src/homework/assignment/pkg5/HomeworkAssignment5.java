package homework.assignment.pkg5;

import java.util.GregorianCalendar;

abstract class Pet {
    GregorianCalendar gc = new GregorianCalendar();
    protected String name = "No name";
    protected int birthyear = 2000;
    
    public Pet(String s, int i) {
        if(s != null) {
            name = s;
        }
        if(i > 1900 || i <= gc.get(gc.YEAR)) {
            birthyear = i;
        }
    }
    
    public Pet(int i) {
        if(i > 1900 || i <= gc.get(gc.YEAR)) {
            birthyear = i;
        }
    }
    
    public String getName() {
        return name;
    }
    
    public int getBirthYear() {
        return birthyear;
    }
    
    public void setName(String s) {
        if(s != null) {
            name = s;
        }
    }
    
    public void setBirthYear(int i) {
        if(i > 1900 || i <= gc.get(gc.YEAR)) {
            birthyear = i;
        }
    }
    
    public int calcAge() {
        return(gc.get(gc.YEAR) - birthyear);
    }
    
    public abstract int convertYears();
    public abstract int lifeExpectancy();
    public abstract String toString();

}

class Cat extends Pet {
    private boolean indoor = true;
    
    public Cat(String nam, int by, boolean in) {
        super(nam, by);
        this.indoor = in;
    }
    
    public boolean getIndoor() {
        return indoor;
    }
    
    public void setIndoor(boolean newIn) {
        indoor = newIn;
    }
    
    public int convertYears() {
        int age = super.calcAge();
        int i, j = 0;
        
        if(age == 1) {
            return 15;
        }
        else if(age == 2) {
            return 24;
        }
        else if(age >= 3) {
            for(i = 0; i <= age - 2; i++) {
                j = j + 4;
            }
            return j + 24;
        }
        return 0;
    }

    public int lifeExpectancy() {
        if (indoor == true) {
            return 16;
        }
        return 4;
    }
    
    public String toString() {
        return "Pet: name= " + super.name + " birthyear= " + 
                super.birthyear + " indoor= " + this.indoor;
    }
}

class Dog extends Pet {
    private int numTricks = 0;
    char size = 'S';
    
    public Dog(String nam, int by, int numT, char si) {
        super(nam, by);
        this.numTricks = numT;
        this.size = si;
    }
    
    public int getNumTricks() {
        return numTricks;
    }
    
    public char getSize() {
        return size;
    }
    
    public void setNumTricks(int numT) {
        numTricks = numT;
    }
    
    public void setSize(char si) {
        size = si;
    }
    
    public int convertYears() {
        int age = super.calcAge();
        int i, j = 0;
        
        if(age == 1) {
            return 9;
        }
        else if(age >= 2) {
            for(i = 0; i <= age - 1; i++) {
                j = j + 7;
            }
            return j + 9;
        }
        return 0;
    }
    
    public int lifeExpectancy() {
        if(size == 'L' || size == 'l') {
            return 10;
        }
        else if(size == 'M' || size == 'm') {
            return 12;
        }
        return 16;
    }
    
    public String toString() {
        return "Pet: name= " + super.name + " birthyear= " + super.birthyear + 
                " size= " + Character.toUpperCase(this.size) + " number of tricks= " + this.numTricks;
    }
}