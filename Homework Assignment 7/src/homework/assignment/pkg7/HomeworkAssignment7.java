/* Homework Assignment 7
 * Eric Schultz
 * 3/25/13
 * Program: Users will be able to play a game of Keno, choosing 
 * 15 out of 80 numbers, 20 of which will be the winning numbers.
 * Winnings are based on the number of numbers chosen correctly.
 */
package homework.assignment.pkg7;
import java.util.Random;

class Spot {
    int value;
    boolean chosen = false;
    
   public Spot(int val) {
       if(val > 0) {
           value = val;
       }
   }
   
   public void Spot() {}
   
   public int getValue() {
       return value;
   }
   
   public boolean getChosen() {
       return chosen;
   }
   
   public void setChosen(boolean c) {
       chosen = c;
   }
}

class KenoBoard {
    Spot kenoSpots[];
    Spot drawnSpots[];
    
    public KenoBoard() {
        kenoSpots = new Spot[KenoGame.MAX_KENO_NUMBER + 1];
        for(int i = 0; i < kenoSpots.length; ++i) {
            kenoSpots[i] = new Spot(i);
        }
        drawnSpots = new Spot[KenoGame.DRAWN_NUMBERS];
    }
    
    public void reset() {
        for(Spot i : kenoSpots) {
            i.chosen = false;
        }
        for(Spot j : drawnSpots) {
            j = null;
        }
    }
    
    public Spot getKenoSpot(int i) {
        if(i > 0 && i <= KenoGame.MAX_KENO_NUMBER) {
            return(kenoSpots[i]);
        }
        else
            return null;
    }
    
    public void chooseSpots() {
        int i = 0;
        Random rng = new Random();
        while(i < drawnSpots.length) {
            int index = rng.nextInt(KenoGame.MAX_KENO_NUMBER);
            if(kenoSpots[index].chosen != true) {
                kenoSpots[index].chosen = true;
                drawnSpots[i] = kenoSpots[index];
                ++i;
            }
        } // end if kenoSpots
    } // end ChooseSpots
    
    public String toString() {
        StringBuilder sb = new StringBuilder();
        String disp = ("");
        for(Spot i : drawnSpots) {
            sb.append((i.value) + " ");
        }
        disp = sb.toString();
        return ("Drawn Spots: " + disp);
    }
} // end kenoBoard

class KenoPlayer {
    Spot markedSpots[];
    int currentIndex = 0;
    int numOfMatches = 0;
    int bet = 1;
    double winnings = 0;
    
    public KenoPlayer(int numSpots, int b) {
        if(numSpots > 0 && numSpots < KenoGame.PAYOFF.length) {
            markedSpots = new Spot[numSpots];
        }
        else {
            markedSpots = new Spot[1];
        }
        if(b > 0) {
            bet = b;
        }
    } // end constructor
    
    public int getMarkedSpotsLength() {
        return markedSpots.length;
    }
    
    public int getNumOfMatches() {
        return numOfMatches;
    }
    
    public int getBet() {
        return bet;
    }
    
    public double getWinnings() {
        return winnings;
    }
    
    boolean setSpot(Spot s) {
        for(int i = 0; i < currentIndex; ++i) {
            if(markedSpots[i].value == s.value) {
                return false;
            }
        }
        if(s != null && currentIndex < markedSpots.length) {
            markedSpots[currentIndex] = s;
            ++currentIndex;
            return true;
        } else {
            return false;
        }
    } // end setSpot
    
    public void calcNumMatches() {
        int count = 0;
        for( Spot i : markedSpots ) {
            if(i.chosen == true) {
                count++;
            }
        }
        numOfMatches = count;
    } // end calcNumMatches
    
    public void calcWinnings() {
        winnings = Math.round(KenoGame.PAYOFF[markedSpots.length][numOfMatches] * bet);
    } // end calcWinnings
    
    public String toString() {
        if(markedSpots != null) {
            StringBuilder sb = new StringBuilder();
            String disp = ("");
            for(Spot i : markedSpots) {
                sb.append((i.value) + " ");
            }
            disp = sb.toString();
            return("Player's Spots: " + disp + " Number of matches: " + getNumOfMatches() + 
                    " Bet: " + getBet() + " Winnings: " + getWinnings());
        }
        else {return("");}
    }
} // end KenoPlayer class

interface NumberGameInterface {
    public abstract void drawNumbers();
    public abstract void getResults();
}

class KenoGame implements NumberGameInterface {
    public static final int MAX_KENO_NUMBER = 80;
    public static final int DRAWN_NUMBERS = 20;
    public static final double PAYOFF[][] = {{0},
        {0, 2.75},
        {0, 1., 5.},
        {0, 0, 2.50, 25.00},
        {0, 0, 1.0, 5.0, 80.0},
        {0, 0, 0, 2.0, 10.0, 600.0},
        {0, 0, 0, 1.0, 8.0, 50., 1499.0},
        {0, 0, 0, 0, 5.0, 10.0, 250., 1500.0},
        {0, 0, 0, 0, 4.00, 8.00, 40.00, 400.00, 10000.00},
        {0, 0, 0, 0, 2.00, 5.00, 20.00, 80.00, 2500.00, 15000.00},
        {0, 0, 0, 0, 0, 2.00, 30.00, 100.00, 500.00, 3000.00, 17500.00},
        {0, 0, 0, 0, 0, 2.00, 15.00, 50.00, 80.00, 800.00, 8000.00, 27500.00},
        {0, 0, 0, 0, 0, 1.00, 5.00, 30.00, 90.00, 500.00, 2500.00, 15000.00, 30000.00},
        {0, 0, 0, 0, 0, 1.00, 2.00, 10.00, 50.00, 500.00, 1000.00, 10000.00, 15000.00, 30000.00},
        {0, 0, 0, 0, 0, 0, 2.00, 8.00, 32.00, 300.00, 800.00, 2500.00, 12000.00, 18000.00, 30000.00},
        {0, 0, 0, 0, 0, 0, 1.00, 7.00, 21.00, 100.00, 400.00, 2000.00, 8000.00, 12000.00, 25000.00, 30000.}};
    KenoBoard board;
    KenoPlayer player;
    
    public KenoGame() {
        board = new KenoBoard();
    } // end constructor
    
    public void newPlayer(int numSpots, int bet) {
        player = new KenoPlayer(numSpots, bet);
    }
    
    public int setOneSpot(int i) {
        if(player != null) {
            if(i > 0 && i <= MAX_KENO_NUMBER) {
                if(player.setSpot(board.getKenoSpot(i))) {
                    return 0;
                } else {return 1;}
            } else {return -1;}
        } else {return -2;}
    } // end setOneSpot
    
    public int getMarkedSpotsLength() {
        return player.markedSpots.length;
    }
    
    public void drawNumbers() {
        board.reset();
        board.chooseSpots();
    }
    
    public void getResults() {
        player.calcNumMatches();
        player.calcWinnings();
    }
} // end KenoGame class