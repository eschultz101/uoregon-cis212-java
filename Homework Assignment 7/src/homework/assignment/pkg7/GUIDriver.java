/* Eric Schultz
 * 3/25/13
 * Keno Game application driver
 */
package homework.assignment.pkg7;

public class GUIDriver {
    public static void main(String [] args) {
        GUILayerHW7 gui = new GUILayerHW7();
        gui.setVisible(true);
    }
}
