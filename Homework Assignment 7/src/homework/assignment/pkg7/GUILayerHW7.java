/* Eric Schultz
 * 3/25/13
 * Keno Game GUI layer code.
 */

package homework.assignment.pkg7;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GUILayerHW7 extends JFrame implements ActionListener {

    public static int numSpots = 1;
    public static int bet = 1;
    public static KenoGame game;
    private JPanel p1, p2, p3;
    private JTextField betTF;
    private JTextField numSpotsTF;
    private JPanel tfPanels;
    private JTextField[] guessTFs;
    private JButton clearTFs, dispResults, getNumSpots, 
            getBet, getIndexes, playAgain;

    public GUILayerHW7() {
        game = new KenoGame();
        setLayout(new BorderLayout());
        p1 = new JPanel(); //for numSpotsTF, betTF, confirm buttons
        getNumSpots = new JButton("Enter Number of Spots");
        getBet = new JButton("Enter Bet");
        betTF = new JTextField(5);
        numSpotsTF = new JTextField(5);
        p2 = new JPanel(); //for clear/disp buttons
        getIndexes = new JButton("Confirm Spots");
        clearTFs = new JButton("Clear Text Fields");
        dispResults = new JButton("Display Results");
        playAgain = new JButton("Play Another Game");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        add(p1, BorderLayout.NORTH);
        add(p2, BorderLayout.SOUTH);
        p1.add(numSpotsTF);
        p1.add(getNumSpots);
        getNumSpots.addActionListener(this);
        p1.add(betTF);
        p1.add(getBet);
        getBet.addActionListener(this);
        p2.add(getIndexes);
        getIndexes.setEnabled(false);
        getIndexes.addActionListener(this);
        p2.add(clearTFs);
        clearTFs.addActionListener(this);
        p2.add(dispResults);
        dispResults.setEnabled(false);
        dispResults.addActionListener(this);
        p2.add(playAgain);
        playAgain.setEnabled(false);
        playAgain.addActionListener(this);
        
        pack();
    }

    public void actionPerformed(ActionEvent e) {
        
        Object obj = e.getSource();
        int index, res;
        if (obj == getNumSpots) {
            getNumSpots();
            getIndexes.setEnabled(true);
        } else if(obj == getBet) {
            getBet();
            JOptionPane.showMessageDialog(tfPanels, "Bet confirmed");
        } else if (obj == getIndexes) {
            game.newPlayer(numSpots, bet);
            int i = 0;
            while(i < numSpots) {
                index = Integer.parseInt(guessTFs[i].getText());
                res = game.setOneSpot(index);
                if (res == -1) {
                    JOptionPane.showMessageDialog(tfPanels, "Input out of range");
                    ++i;
                } else if (res == 1) {
                    JOptionPane.showMessageDialog(tfPanels, "Input already chosen");
                    ++i;
                } else if (res == 0) {
                    ++i;
                }
            }
            JOptionPane.showMessageDialog(tfPanels, "Spot choices confirmed - Click Display Results to see if you won!");
            dispResults.setEnabled(true);
        } else if (obj == clearTFs) {
            numSpotsTF.setText("");
            betTF.setText("");
            for (int j = 0; j < numSpots; ++j) {
                guessTFs[j].setText("");
            }
        } else if (obj == dispResults) {
            game.drawNumbers();
            game.getResults();
            p3 = new JPanel();
            JLabel results = new JLabel("<html>" + game.board.toString() + 
                                    "<br>" + game.player.toString() + "</html>");
            p3.add(results);
            add(p3, BorderLayout.EAST);
            playAgain.setEnabled(true);
            pack();
        } else if (obj == playAgain) {
           GUILayerHW7 newGame = new GUILayerHW7();
           this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
           newGame.setVisible(true);
        }
    } // end actionPerformed

    public void getNumSpots() {
        int num = Integer.parseInt(numSpotsTF.getText());
        if (num > 0 && num < KenoGame.PAYOFF.length) {
            numSpots = num;
        }
        tfPanels = new JPanel(new GridLayout(numSpots, 1, 5, 5));
        guessTFs = new JTextField[numSpots];
        for (int i = 0; i < numSpots; ++i) {
            guessTFs[i] = new JTextField(10);
            tfPanels.add(guessTFs[i]);
        }
        add(tfPanels, BorderLayout.WEST);
        pack();
    } // end getNumSpots
    
    public void getBet() {
        int b = Integer.parseInt(betTF.getText());
        if (b > 0) {
           bet = b;
        }
    } // end getBet
} // end GUILayerHW7 class
