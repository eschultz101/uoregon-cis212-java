package exercise.pkg7.pkg2;

public class Exercise72 {
    public static void main(String[] args) {
        Employee[] empArray = new Employee[]{
            new Hourly("Donald Duck", "123-45-6789", 20., 30.),
            new Salaried("Mickey Mouse", "987-65-4321", 100000.),
            new Salaried("Bugs Bunny", "121-21-2121", 90000.),
            new Hourly("Elmer Fudd", "343-43-4343", 10., 15.)};
        
        for(Employee i: empArray) {
            System.out.println(i.toString() + i.computePay());
        } // for
    } // main
}

abstract class Employee {

    protected String name;
    protected String ssn;

    public Employee(String n, String s) {
        name = n;
        ssn = s;
    }

    public String getName() {
        return name;
    }

    public String getSsn() {
        return ssn;
    }
    // could add mutators

    public String toString() {

        return "Employee: name= " + name
                + ", SSN= " + ssn;

    }
    
    public abstract double computePay();
    
} // end class Employee

class Salaried extends Employee {

    private double salary;
    private static int payPeriods = 12;

    public Salaried(String name, String ssn, double s) {
        super(name, ssn);
        salary = s;
    }

    public void setSalary(double sal) {
        salary = sal;
    }

    public double getSalary() {
        return salary;
    }

    public String toString() {
        return "Salaried " + super.toString() + ", Salary= " + salary;
    }
    
    public double computePay(){
        return Math.round(salary/payPeriods*100.)/100.;
    }
} // end class Salaried

class Hourly extends Employee {
    
    private double hourlyRate;
    private double hoursWorked;
    
    public Hourly(String name, String ssn, double hr, double hw) {
        super(name, ssn);
        hourlyRate = hr;
        hoursWorked = hw;
    }
    
    public double getHourlyRate(){
        return hourlyRate;
    }
    
    public void setHourlyRate(double hRate){
        hourlyRate = hRate;
    }
    
    public double getHoursWorked() {
        return hoursWorked;
    }
    
    public void setHoursWorked(double hWorked) {
        hoursWorked = hWorked;
    }
    
    public String toString() {
        return "Hourly " + super.toString() + ", Hourly Rate= " + hourlyRate;
    }
    public double computePay() {
        return(hourlyRate*hoursWorked);
    }
}