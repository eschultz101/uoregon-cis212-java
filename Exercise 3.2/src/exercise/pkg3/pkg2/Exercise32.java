package exercise.pkg3.pkg2;

import java.util.Scanner;

public class Exercise32 {
    
    static Scanner scanner = new Scanner(System.in); 
    
    public static void main(String[] args) {
        
        int[] num;
        num = new int[3];
        int avg, largest;
        
        System.out.println("Enter three integers to be used:");
        
        num[0] = scanner.nextInt();
        num[1] = scanner.nextInt();
        num[2] = scanner.nextInt();
        
        avg = averager(num);
        largest = large(num);
        
        System.out.println("Average: " + avg + ", Largest number: " + largest);
        
    }
    
    public static int averager(int num[])
    {
        int avg;
        
        avg = (num[0] + num[1] + num[2])/3;
        
        return avg;
    }
    
    public static int large(int num[])
    {
        int largest;
        
        largest = num[0];
        
        for(int i = 0; i < 3; i++)
        {
            if(num[i] > largest)
            {
                largest = num[i];
            }
        }
        return largest;
    }
}
