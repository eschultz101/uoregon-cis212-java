import java.util.Arrays;


public class Main {
	
	public static void main(String[] args) {
		
		int f = factorial(4);
		System.out.println("factorial Output: " + f);
		
		int mysteryAns = mystery(1, 20);
		System.out.println("mystery Output: " + mysteryAns);
		
		int sumAns = sum(4);
		System.out.println("sum Output: " + sumAns);
		
		double powerAns = power(3, 4);
		System.out.println("power Output: " + powerAns);
		
		int[] i = {5,5,7,2,1};
		int min = recursiveMinimum(i);
		System.out.println("recursiveMinimum Output: " + min);
		
		int keyLinear = linearSearch(i, 7);
		int keyRecLinear1 = recursiveLinearSearch(i, 7);
		System.out.println("linearSearch Output: " + keyLinear);
		System.out.println("recursiveLinearSearch Output: " + keyRecLinear1);
		
		int keyRecLinear2 = recursiveLinearSearch(i, 1, i.length - 1);
		System.out.println("recursiveLinearSearch Output: " + keyRecLinear2);
		
	}
	
	public static int factorial(int a) {
		if(a <= 1) {
			return 1;
		} else {
			return a * factorial(a - 1);
		}
	}
	
	//What does the following code do? (Exercise 18.7)
	//In other words, what should the name of this method be?
	public static int mystery(int a, int b) {
		
		if(b == 1) {
			return a;
		} else {
			return a + mystery(a, b - 1);
		}
	}
	
	//Find the error(s) in the following recursive method, and explain
	//how to correct it (them).  This method should find the sum of
	//the values from 0 to n.
	public static int sum(int n) {
		
		if(n == 0) {
			return 0;
		} else {
			return n + sum(n - 1);
		}
	}
	
	//Write a recursive method power(base, exponent) that, when
	//called, returns base^exponent.  Assume that exponent is an
	//integer greater than or equal to 1.
	public static double power(double base, int exponent) {
		
		if(exponent == 0) {
			return 1;
		} else {
			return base * power(base, exponent - 1);
		}
	}
	
	//Write a recursive method recursiveMinimum that determines
	//the smallest element in an array of integers.  The method
	//should return when it receives an array of one element.  Use
	//the static method Arrays.copyOfRange to resize the array.
	public static int recursiveMinimum(int[] a) {
		
		if(a.length == 1) {
			return a[0];
		} else {
		    return Math.min(a[a.length - 1], 
		    	   recursiveMinimum(Arrays.copyOfRange(a, 0, a.length - 1)));
		}
	}
	
	//Performs a linear search on the data.
	public static int linearSearch(int[] data, int searchKey) {
		
		for(int i = 0; i < data.length; i++) {
			if(data[i] == searchKey) {
				return i;
			}
		}
		return -1;
		
	}
	
	//Performs a recursive linear search on the data.
	public static int recursiveLinearSearch(int[] data, int searchKey) {
		
		if(data.length == 0) {
			return -1;
		} else if(data[data.length - 1] == searchKey) {
			return data.length - 1;
		} else {
			return recursiveLinearSearch(Arrays.copyOfRange(data, 0, data.length - 1),
					               searchKey);
		}
	}
	
	//Performs a recursive linear search on the data, but without altering or
	//resizing the data array.
	public static int recursiveLinearSearch(int[] data, int searchKey, int endIndex) {
		if(endIndex == 0) {
			return -1;
		} else if(data[endIndex] == searchKey) {
			return endIndex;
		} else {
			return recursiveLinearSearch(data, searchKey, endIndex - 1);
		}
	}
	

}
