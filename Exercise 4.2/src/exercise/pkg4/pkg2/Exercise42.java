package exercise.pkg4.pkg2;

public class Exercise42 {
    public static void main(String[] args) {

        int[][] array;
        
        array = allocator(new int[]{1, 2, 3, 4});
        
        initializer(array);
        
    }
    
    public static int[][] allocator(int[] array) {

        int[][] dArray;
        
        dArray = new int[array.length][array.length];

        return dArray;
    }
    
    public static void initializer(int[][] array) {
        
        for(int row = 0; row < array.length; row++) {
            for(int col = 0; col < array[row].length; col++) {
                array[row][col] = row + col;
                System.out.printf(array[row][col] + " ");
            }System.out.printf("\n");
        } 
    }
}

/*
run:
0 1 2 3 
1 2 3 4 
2 3 4 5 
3 4 5 6 
BUILD SUCCESSFUL (total time: 0 seconds)
*/