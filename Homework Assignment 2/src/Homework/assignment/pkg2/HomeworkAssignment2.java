package Homework.assignment.pkg2;

import java.util.Scanner;

//*******************************************************************************
public class HomeworkAssignment2 {

    public static void main(String[] args) {
        int rows, cols, numSpots;
        boolean[][] gameGrid, guess, results;
        boolean play = true;

        System.out.println("Enter number of rows: ");
        rows = inputMethods.getDim();
        System.out.println("Enter number of columns: ");
        cols = inputMethods.getDim();

        gameGrid = allocateGrid(rows, cols);
        guess = allocateGrid(rows, cols);
        results = allocateGrid(rows, cols);

        numSpots = (rows + cols) / 2;

        while (play == true) {
            setRandomValues(gameGrid, numSpots, rows, cols);

            setInputValues(guess, numSpots, rows, cols);

            compareGrids(gameGrid, guess, results);

            System.out.println("Your guesses: ");
            displayGrid(guess, 'G');
            System.out.println("Game's mines: ");
            displayGrid(gameGrid, '*');
            System.out.println("Your matches: ");
            displayGrid(results, 'M');

            play = inputMethods.repeat();
        }
    }
    public static boolean [][] allocateGrid(int rows, int cols) {
        
        if(rows <= 0) {
            rows = 1;
        }
        if(cols <= 0) {
            cols = 1;
        }
        return new boolean[rows][cols]; 
    }
    
    public static void setRandomValues(boolean[][] gameGrid, int numSpots, int rows, int cols) {
        initGrid(gameGrid);

        int rowIndex, colIndex;

        for (int i = 0; i < numSpots; ++i) {

            rowIndex = (int)(Math.random() * (rows - 1) - 0 + 1) + 0;
            colIndex = (int)(Math.random() * (cols - 1) - 0 + 1) + 0;

            if (gameGrid[rowIndex][colIndex] != true) {
                gameGrid[rowIndex][colIndex] = true;
            }
        }
    }
    
    public static void initGrid(boolean[][] array) {

        for(int i = 0; i < array.length; ++i) {
            for(int j = 0; j < array[i].length; ++j) {
                array[i][j] = false;
            }
        }
    }
    
    public static void setInputValues(boolean[][] guess, int numSpots, int rows, int cols) {
        initGrid(guess);
        
        int rowGuess, colGuess;
        
        for(int i = 0; i < numSpots; ++i) {
            
            rowGuess = inputMethods.readRow(rows);
            colGuess = inputMethods.readCol(cols);
            
            if(guess[rowGuess][colGuess] != true) {
                guess[rowGuess][colGuess] = true;
            }
        
        }
    }
    
    public static void compareGrids(boolean[][] gameGrid, boolean[][] guess, boolean[][] results) {
        
        for(int i = 0; i < gameGrid.length; ++i) {
            for(int j = 0; j < gameGrid[i].length; ++j) {
                if(gameGrid[i][j] && guess[i][j] == true) {
                    results[i][j] = true;
                }
                else {
                    results[i][j] = false;
                }
            }
        }
    }
    
    public static void displayGrid(boolean[][] array, char marker) {
        
        for(int i = 0; i < array.length; ++i) {
            for(int j = 0; j < array[i].length; ++j) {
                if(array[i][j] == true) {
                    System.out.print(marker);
                }
                else {
                    System.out.print(" ");
                }
            } System.out.print("\n");
        }
    }
}
class inputMethods {

    static Scanner scanner = new Scanner(System.in);

    public static int getDim() {
        int num;

        num = scanner.nextInt();

        while (num < 5 || num > 20) {
            System.out.println("Invalid dimension.  Enter new dimension: ");
            num = scanner.nextInt();
        }

        return num;
    }

    public static int readRow(int rows) {
        int num;
        System.out.println("Enter row number to guess: ");
        num = scanner.nextInt();

        while (num < 0 || num >= rows) {
            System.out.println("Invalid entry.  Enter row number to guess: ");
            num = scanner.nextInt();
        }

        return num;
    }
    
    public static int readCol(int cols) {
        int num;
        System.out.println("Enter column number to guess: ");
        num = scanner.nextInt();
        
        while(num < 0 || num >= cols) {
            System.out.println("Invalid entry.  Enter column number to guess: ");
            num = scanner.nextInt();
        }
        
        return num;
    }
    
    public static boolean repeat() {
        String response;
        
        System.out.println("Do you want to play again?");
        response = scanner.next();
        
        if(response.charAt(0) == 'Y' || response.charAt(0) == 'y') {
            return true;
        }
        else {
            return false;
        }
    }
}

/*     RUN 1
Enter number of rows: 
5
Enter number of columns: 
5
Enter row number to guess: 
1
Enter column number to guess: 
1
Enter row number to guess: 
3
Enter column number to guess: 
4
Enter row number to guess: 
5
Invalid entry.  Enter row number to guess: 
6
Invalid entry.  Enter row number to guess: 
2
Enter column number to guess: 
4
Enter row number to guess: 
3
Enter column number to guess: 
6
Invalid entry.  Enter column number to guess: 
1
Enter row number to guess: 
3
Enter column number to guess: 
5
Invalid entry.  Enter column number to guess: 
4
Your guesses: 
     
 G   
    G
 G  G
     
Game's mines: 
     
 *  *
     
   * 
 * * 
Your matches: 
     
 M   
     
     
     
Do you want to play again?
yes
Enter row number to guess: 
1
Enter column number to guess: 
3
Enter row number to guess: 
0
Enter column number to guess: 
4
Enter row number to guess: 
2
Enter column number to guess: 
0
Enter row number to guess: 
4
Enter column number to guess: 
1
Enter row number to guess: 
2
Enter column number to guess: 
5
Invalid entry.  Enter column number to guess: 
4
Your guesses: 
    G
   G 
G   G
     
 G   
Game's mines: 
     
 **  
     
 * **
     
Your matches: 
     
     
     
     
     
Do you want to play again?
yes
Enter row number to guess: 
2
Enter column number to guess: 
5
Invalid entry.  Enter column number to guess: 
4
Enter row number to guess: 
0
Enter column number to guess: 
1
Enter row number to guess: 
2
Enter column number to guess: 
0
Enter row number to guess: 
4
Enter column number to guess: 
3
Enter row number to guess: 
4
Enter column number to guess: 
4
Your guesses: 
 G   
     
G   G
     
   GG
Game's mines: 
     
 *  *
   * 
   * 
    *
Your matches: 
     
     
     
     
    M
Do you want to play again?
no
BUILD SUCCESSFUL (total time: 3 minutes 16 seconds)
*/

/*    RUN 2
Enter number of rows: 
23
Invalid dimension.  Enter new dimension: 
12
Enter number of columns: 
3
Invalid dimension.  Enter new dimension: 
4
Invalid dimension.  Enter new dimension: 
6
Enter row number to guess: 
0
Enter column number to guess: 
2
Enter row number to guess: 
3
Enter column number to guess: 
4
Enter row number to guess: 
5
Enter column number to guess: 
2
Enter row number to guess: 
0
Enter column number to guess: 
1
Enter row number to guess: 
10
Enter column number to guess: 
9
Invalid entry.  Enter column number to guess: 
6
Invalid entry.  Enter column number to guess: 
4
Enter row number to guess: 
12
Invalid entry.  Enter row number to guess: 
11
Enter column number to guess: 
1
Enter row number to guess: 
6
Enter column number to guess: 
0
Enter row number to guess: 
0
Enter column number to guess: 
3
Enter row number to guess: 
1
Enter column number to guess: 
2
Your guesses: 
 GGG  
  G   
      
    G 
      
  G   
G     
      
      
      
    G 
 G    
Game's mines: 
      
  *  *
 *    
  *   
      
      
   * *
   *  
      
      
    * 
      
Your matches: 
      
  M   
      
      
      
      
      
      
      
      
    M 
      
Do you want to play again?
no
BUILD SUCCESSFUL (total time: 1 minute 14 seconds)
*/