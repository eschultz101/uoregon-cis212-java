
import javax.swing.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Package extends JFrame {

	private static final long serialVersionUID = 1L;
	private JCheckBox chBox1 = new JCheckBox();
	private JCheckBox chBox2 = new JCheckBox();
	private JCheckBox chBoxBoat = new JCheckBox();
	private JTextField date1 = new JTextField();
	private JTextField date2 = new JTextField();
	private Calendar firstDate;
	private Calendar endDate;
	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy",Locale.US);
	private JButton calc = new JButton("Calculate cost");
	private JTextField display = new JTextField();
	private JLabel label1bed = new JLabel("One Bedroom");
	private JLabel label2bed = new JLabel("Two Bedrooms");
	private JLabel labelBoat = new JLabel("Rowboat rental?");
	private JLabel labelDate1 = new JLabel("Enter beginning date: ");
	private JLabel labelDate2 = new JLabel("Enter end date: ");
	
	public Package(){
		add(chBox1);
		add(label1bed);
		add(chBox2);
		add(label2bed);
		chBox1.addItemListener(new CheckBoxHandler());
		chBox2.addItemListener(new CheckBoxHandler());
		add(chBoxBoat);
		add(labelBoat);
		add(labelDate1);
		add(date1);
		add(labelDate2);
		add(date2);
		add(calc);
		add(display);
	}
	
	public static void main(String [] args) {
		Package driver = new Package();
		driver.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void ActionPerformed(ActionEvent e) {
		String temp;
		double result;
		Object obj = e.getSource();
		if(obj == calc) {
			try{
				firstDate.setTime(sdf.parse(date1.getText()));
			} catch(Exception f){
				System.out.println("Unable to parse date stamp");
			}
			try{
				endDate.setTime(sdf.parse(date2.getText()));
			} catch(Exception f){
				System.out.println("Unable to parse date stamp");
				
			}
			result = Price(daysBetween(firstDate, endDate));
			temp = String.valueOf(result);
			display.setText(temp);
		}
	}
	
	public static double daysBetween(Calendar startDate, Calendar endDate) {  
		  Calendar date = (Calendar) startDate.clone();  
		  long daysBetween = 0;  
		  while (date.before(endDate)) {  
		    date.add(Calendar.DAY_OF_MONTH, 1);  
		    daysBetween++;  
		  }  
		  return daysBetween/7;  
		}
	
	public double Price(double daysBetween){
		double x = 600.00;
		double y = 0.00;
		if (chBox2.isSelected()){
			x=850.00;
		}
		if(chBoxBoat.isSelected()){
			y=60.00;
		}
		return daysBetween*(x+y); 
	}
	
	class CheckBoxHandler implements ItemListener {
		public void itemStateChanged( ItemEvent e) {
			if( chBox1.isSelected()) {
				chBox2.setSelected(false);
			}
			if( chBox2.isSelected()) {
				chBox1.setSelected(false);
			}
		}
	}
}


