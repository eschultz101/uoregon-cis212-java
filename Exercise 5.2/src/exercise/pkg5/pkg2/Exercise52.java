package exercise.pkg5.pkg2;

public class Exercise52 {
    public static void main( String [] args) {
        Rectangle r1 = new Rectangle(2., 2.);
        Rectangle r2 = new Rectangle(3., 5.);
        
        System.out.println("Area of r1: " + r1.getArea());
        System.out.println("Perimeter of r2: " + r2.getPerimeter());
        System.out.println("Length of r1: " + r1.getLength());
        System.out.println("Width of r1: " + r1.getWidth());
        System.out.println("Length of r2: " + r2.getLength());
        System.out.println("Width of r2: " + r2.getWidth());
        
    }
}

class Rectangle {
       private double length=1.;
       private double width=1.;
       private double perimeter=4.;
       private double area=1.;

       public Rectangle(double len, double wid){
              if(len > 0){
                     length = len;
                     calcAreaAndPerimeter();
              }
              // else leave at 1
      
              // another way to do this:
              setWidth(wid);//if didn't work, leave as 1            
       } // end constructor

       public Rectangle(){ }// does nothing

       private void calcAreaAndPerimeter(){
              perimeter = 2.*(length+width);
              area = length * width;
       } // end calcAreaAndPerimeter

       public double getLength(){ return length; }

       public double getWidth(){ return width; }

       public double getPerimeter(){ return perimeter; }

       public double getArea(){ return area; }

       public boolean setLength(double len){
              if( len > 0 ){
                     length= len;
                     calcAreaAndPerimeter();
                     return true;
              }// end if
              // DON'T print an error message
              return false;
       } // end setLength

       public boolean setWidth(double wid){
              if( wid > 0 ){
                     width = wid;
                     calcAreaAndPerimeter();
                     return true;
              }// end if
              return false;
       } // end setWidth

       public void display(){
            System.out.println("Rectangle: length= "+
                  length + ", width= " + width +
                  ", perimeter= "+ perimeter +
                  ", area= " + area);
      } // end display
} // end class Rectangle
