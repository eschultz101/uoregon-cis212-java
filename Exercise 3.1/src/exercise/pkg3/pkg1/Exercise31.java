package exercise.pkg3.pkg1;

class MyMath {
    public static double round100th(double toRound) {
        toRound = toRound / 100;
        toRound = Math.round(toRound);
        toRound = toRound * 100;
        return toRound;
    }
} //MyMath Class

class TryMyMath {
    public static void main(String[] args) {
        printNum();
    }
    public static void printNum() {
        double num, square, cube;
        num = 1234;
        
        num = MyMath.round100th(num);

        square = num * num;
        cube = square * num;

        System.out.printf("%.2f %.2f %.2f\n", num, square, cube);
    }
} // TryMymath Class

/* Output
 * 
 * run:
 * 1200.00 1440000.00 1728000000.00
 * BUILD SUCCESSFUL (total time: 0 seconds)
 * 
 */