package exercise.pkg2.pkg4;

public class Exercise24 {
    public static void main(String[] args) {
        int number;
        double square, cube;
        
        System.out.printf("\tNUMBER\t\tSQUARE\t\tCUBE\n");
        for(number = 2; number <=20; number++)
        {
            square = number*number;
            cube = number*number*number;
            System.out.printf("%10d\t%10.0f\t%10.0f\n", number, square, cube);
        } // for loop
        
    }
}
