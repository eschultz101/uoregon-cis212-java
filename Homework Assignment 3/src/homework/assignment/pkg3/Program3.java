
package homework.assignment.pkg3;

public class Program3 {
    public static void main(String[] args) {
        int rows, cols, numSpots;
        MineGrid userGrid, gameGrid, resultsGrid;
        boolean play = true;
        
        System.out.println("Enter number of rows: ");
        rows = inputMethods.getDim();
        System.out.println("Enter number of columns: ");
        cols = inputMethods.getDim();
        
        userGrid = new MineGrid(rows, cols);
        
        gameGrid = new MineGrid(rows, cols);
       
        numSpots = (rows + cols)/2;
        
        while (play == true) {
            
            gameGrid.setRands(numSpots);

            setInputs(userGrid, numSpots, rows, cols);

            resultsGrid = gameGrid.compareMineGrid(userGrid);

            System.out.println("Your guesses: ");
            userGrid.displayMineGrid('G');
            System.out.println("Game's mines: ");
            gameGrid.displayMineGrid('*');
            System.out.println("Your matches: ");
            resultsGrid.displayMineGrid('M');

            play = inputMethods.repeat();
        }
    }

    public static void setInputs(MineGrid userGrid, int numSpots, int rows, int cols) {
        userGrid.initGrid();

        int rowGuess, colGuess;

        for (int i = 0; i < numSpots; ++i) {

            rowGuess = inputMethods.readRow(rows);
            colGuess = inputMethods.readCol(cols);

            if (userGrid.getGridValue(rowGuess, colGuess) != true) {
                userGrid.setTrue(rowGuess, colGuess);
            }
        }
    }
    
}
