package homework.assignment.pkg3;

import java.util.Scanner;

//*******************************************************************************
public class HomeworkAssignment3 {

    public static void main(String[] args) {
        int rows, cols, numSpots;
        boolean[][] gameGrid, guess, results;
        boolean play = true;

        System.out.println("Enter number of rows: ");
        rows = inputMethods.getDim();
        System.out.println("Enter number of columns: ");
        cols = inputMethods.getDim();

        gameGrid = allocateGrid(rows, cols);
        guess = allocateGrid(rows, cols);
        results = allocateGrid(rows, cols);

        numSpots = (rows + cols) / 2;

        while (play == true) {
            setRandomValues(gameGrid, numSpots, rows, cols);

            setInputValues(guess, numSpots, rows, cols);

            compareGrids(gameGrid, guess, results);

            System.out.println("Your guesses: ");
            displayGrid(guess, 'G');
            System.out.println("Game's mines: ");
            displayGrid(gameGrid, '*');
            System.out.println("Your matches: ");
            displayGrid(results, 'M');

            play = inputMethods.repeat();
        }
    }
    public static boolean [][] allocateGrid(int rows, int cols) {
        
        if(rows <= 0) {
            rows = 1;
        }
        if(cols <= 0) {
            cols = 1;
        }
        return new boolean[rows][cols]; 
    }
    
    public static void setRandomValues(boolean[][] gameGrid, int numSpots, int rows, int cols) {
        initGrid(gameGrid);

        int rowIndex, colIndex;

        for (int i = 0; i < numSpots; ++i) {

            rowIndex = (int)(Math.random() * (rows - 1) - 0 + 1) + 0;
            colIndex = (int)(Math.random() * (cols - 1) - 0 + 1) + 0;

            if (gameGrid[rowIndex][colIndex] != true) {
                gameGrid[rowIndex][colIndex] = true;
            }
        }
    }
    
    public static void initGrid(boolean[][] array) {

        for(int i = 0; i < array.length; ++i) {
            for(int j = 0; j < array[i].length; ++j) {
                array[i][j] = false;
            }
        }
    }
    
    public static void setInputValues(boolean[][] guess, int numSpots, int rows, int cols) {
        initGrid(guess);
        
        int rowGuess, colGuess;
        
        for(int i = 0; i < numSpots; ++i) {
            
            rowGuess = inputMethods.readRow(rows);
            colGuess = inputMethods.readCol(cols);
            
            if(guess[rowGuess][colGuess] != true) {
                guess[rowGuess][colGuess] = true;
            }
        
        }
    }
    
    public static void compareGrids(boolean[][] gameGrid, boolean[][] guess, boolean[][] results) {
        
        for(int i = 0; i < gameGrid.length; ++i) {
            for(int j = 0; j < gameGrid[i].length; ++j) {
                if(gameGrid[i][j] && guess[i][j] == true) {
                    results[i][j] = true;
                }
                else {
                    results[i][j] = false;
                }
            }
        }
    }
    
    public static void displayGrid(boolean[][] array, char marker) {
        
        for(int i = 0; i < array.length; ++i) {
            for(int j = 0; j < array[i].length; ++j) {
                if(array[i][j] == true) {
                    System.out.print(marker);
                }
                else {
                    System.out.print(" ");
                }
            } System.out.print("\n");
        }
    }
}
//****************************************************************************************
class inputMethods {

    static Scanner scanner = new Scanner(System.in);

    public static int getDim() {
        int num;

        num = scanner.nextInt();

        while (num < 5 || num > 20) {
            System.out.println("Invalid dimension.  Enter new dimension: ");
            num = scanner.nextInt();
        }

        return num;
    }

    public static int readRow(int rows) {
        int num;
        System.out.println("Enter row number to guess: ");
        num = scanner.nextInt();

        while (num < 0 || num >= rows) {
            System.out.println("Invalid entry.  Enter row number to guess: ");
            num = scanner.nextInt();
        }

        return num;
    }
    
    public static int readCol(int cols) {
        int num;
        System.out.println("Enter column number to guess: ");
        num = scanner.nextInt();
        
        while(num < 0 || num >= cols) {
            System.out.println("Invalid entry.  Enter column number to guess: ");
            num = scanner.nextInt();
        }
        
        return num;
    }
    
    public static boolean repeat() {
        String response;
        
        System.out.println("Do you want to play again?");
        response = scanner.next();
        
        if(response.charAt(0) == 'Y' || response.charAt(0) == 'y') {
            return true;
        }
        else {
            return false;
        }
    }
}
//***************************************************************************************
class MineGrid {
    private boolean[][] grid;
    static final int MIN_DIM = 5;
    static final int MAX_DIM = 20;
    
    public MineGrid(int dim1, int dim2) {
        if(dim1 > MAX_DIM) {
            dim1 = 20;
        }
        else if(dim1 < MIN_DIM) {
            dim1 = 5;
        }
        
        if(dim2 > MAX_DIM) {
            dim2 = 20;
        }
        else if(dim2 < MIN_DIM) {
            dim2 = 5;
        }
        
        grid = new boolean[dim1][dim2];
        
    }
    
    public MineGrid(boolean[][] temp) {
        int i = 0;
        boolean flag = true;
        if (grid != null) {
            if (grid.length <= MAX_DIM && grid.length >= MIN_DIM && grid[0].length >= MIN_DIM && grid[0].length <= MAX_DIM) {
                while (i < grid.length && flag == true) {
                    if (grid[0].length != grid[i].length) {
                        flag = false;
                    }
                    ++i;
                }
            }
        }
        if(flag == true){
            temp = grid;
        }
        else {
            temp = new boolean[MIN_DIM][MIN_DIM];
        }
    }
    
    public int getRows() {
        return(grid.length);
    }
    
    public int getCols() {
        return(grid[0].length);
    }
    
    public void setRands(int numSpots) {
        initGrid();
        int rowIndex, colIndex;

        for (int i = 0; i < numSpots; ++i) {

            rowIndex = (int)(Math.random() * (grid.length - 1) - 0 + 1) + 0;
            colIndex = (int)(Math.random() * (grid[0].length - 1) - 0 + 1) + 0;

            if (grid[rowIndex][colIndex] != true) {
                grid[rowIndex][colIndex] = true;
            }
        }
    }
    
    public void initGrid() {
        for(int i = 0; i < grid.length; ++i) {
            for(int j = 0; j < grid[0].length; ++j) {
                grid[i][j] = false;
            }
        }
    }
    
    public boolean setTrue(int rowIndex, int colIndex) {
        
        if(rowIndex >= MIN_DIM && rowIndex <= MAX_DIM && colIndex >= MIN_DIM && colIndex <= MAX_DIM) {
            grid[rowIndex][colIndex] = true;
            return true;
        }
        return false;
    }
    
    public boolean getGridValue(int rowIndex, int colIndex) {
        if(rowIndex >= MIN_DIM && rowIndex <= MAX_DIM && colIndex >= MIN_DIM && colIndex <= MAX_DIM) {
            return grid[rowIndex][colIndex];
        }
        return false;
    }
    
    public MineGrid compareMineGrid(MineGrid comp) {
        boolean[][] results = new boolean[grid.length][grid[0].length];
        
        if (this.grid.length == comp.grid.length && this.grid[0].length == comp.grid[0].length) {
            for (int i = 0; i < grid.length; ++i) {
                for (int j = 0; j < grid[i].length; ++j) {
                    if (this.grid[i][j] && comp.grid[i][j] == true) {
                        results[i][j] = true;
                    } else {
                        results[i][j] = false;
                    }
                }
            }  return new MineGrid(results);
        }
        else {
            return new MineGrid(MIN_DIM, MIN_DIM);
        }
    }

    public void displayMineGrid(char marker) {
        for (int i = 0; i < grid.length; ++i) {
            for (int j = 0; j < grid[i].length; ++j) {
                if (grid[i][j] == true) {
                    System.out.print(marker);
                } else {
                    System.out.print(" ");
                }
            }
            System.out.print("\n");
        }
    }
}