package exercise.pkg7.pkg1;

class Employee {

    protected String name;
    protected String ssn;

    public Employee(String n, String s) {
        name = n;
        ssn = s;
    }

    public String getName() {
        return name;
    }

    public String getSsn() {
        return ssn;
    }
    // could add mutators

    public String toString() {

        return "Employee: name=" + name
                + ", SSN= " + ssn;

    }
} // end class Employee

class Salaried extends Employee {

    private double salary;

    public Salaried(String name, String ssn, double s) {
        super(name, ssn);
        salary = s;
    }

    public void setSalary(double sal) {

        salary = sal;

    }

    public double getSalary() {
        return salary;
    }

    public String toString() {
        return "Salaried " + super.toString() + ", Salary=" + salary;
    }
} // end class Salaried
/* 
-------------------------------------------------------------------
*  Hourly UML Diagram  <<subclass of Employee>>                   *
* -----------------------------------------------------------------
* -hourlyRate: double                                             *
* -----------------------------------------------------------------
* +hourlyRate(name: String, ssn: String, hr: double)              *
* +getHourlyRate: double (accessor)                               *
* +setHourlyRate(): void (mutator)                                *
* +toString(): String                                             *
-------------------------------------------------------------------
 */
class Hourly extends Employee {
    
    private double hourlyRate;
    
    public Hourly(String name, String ssn, double hr) {
        super(name, ssn);
        hourlyRate = hr;
    }
    
    public double getHourlyRate(){
        return hourlyRate;
    }
    
    public void setHourlyRate(double hRate){
        hourlyRate = hRate;
    }
    
    public String toString() {
        return "Hourly " + super.toString() + ", Hourly Rate=" + hourlyRate;
    }
}