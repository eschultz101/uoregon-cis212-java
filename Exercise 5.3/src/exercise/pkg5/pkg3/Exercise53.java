package exercise.pkg5.pkg3;

public class Exercise53 {
    public static void main(String[] args) {
        
    }

}
/*
UML Diagram
--------------------------------------------------------------------------------
|                                   Customer                                   |                       
--------------------------------------------------------------------------------
| -name: String                                                                |
| -acctNum: long                                                               |
| -savAcct: SavingsAccount                                                     |
| ------------------------------------------------------------------------------
| +Customer(tempStr:String, tempInt:long, tempDouble:double) <<constructor>>   |
| +getName(): String                                                           |
| +getNum(): long                                                              |
| +getAcct(): SavingsAccount                                                   |
| +setName(): void                                                             |
--------------------------------------------------------------------------------
*/
class Customer {
    private String name;
    private long acctNum;
    private SavingsAccount savAcct;
    
    public Customer(String tempStr, long tempInt, double tempDouble) {
        name = tempStr;
        acctNum = tempInt;
        savAcct = new SavingsAccount(tempDouble);
    }
    
    public String getName() {
        return name;
    }
    
    public long getNum() {
        return acctNum;
    }
    
    public SavingsAccount getAcct() {
        return savAcct;
    }
    
    public void setName() {
        this.name = name;
    }
}

class SavingsAccount {

    private static double annualIntRate; // default 0.
    private double balance; // default 0.

    public SavingsAccount(double bal) {

        if (bal > 0) {
            balance = bal;
        }
    // else leave as 0

    } // end constructor

    public SavingsAccount() {
    }//default const.

    public double getBalance() {
        return balance;
    }

    public boolean transaction(double amount) {

        if (balance + amount >= 0) {

            balance += amount;
            return true;

        }// end if
        return false;

    } // end transaction

    public double addMonthlyInterest() {

        double interest;

        interest = balance * annualIntRate / 12.;
// should be rounded to nearest 100th
        balance += interest;
        return interest;

    } // end addMonthlyInterest

    public static boolean modifyIntRate(double newRate) {

        if (newRate >= 0) {

            annualIntRate = newRate;
            return true;

        }// end if

        return false;

    } // end modifyIntRate

    public static double getAnnualIntRate() {

        return annualIntRate;

    } // end getAnnualIntRate
} // end SavingsAccount