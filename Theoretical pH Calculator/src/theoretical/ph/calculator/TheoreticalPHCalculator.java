package theoretical.ph.calculator;
import java.math.*;
import java.text.*;
import java.util.Scanner;

public class TheoreticalPHCalculator {
    
    static Scanner sc = new Scanner(System.in);
    static DecimalFormat df = new DecimalFormat("#.##");
    
    public static void main(String[] args) {
        String again = "y";
        double k;
        String choice;
        double temp;
        double pH;
        double pOH;
        
        while(again.charAt(0) == 'y') {
            System.out.println("Entering a Ka or Kb? (a for Ka, b for Kb): ");
            choice = sc.next();
            
            if(choice.charAt(0) == 'a') {
                System.out.println("Enter Ka of substance (in the form #.##e#): ");
                k = sc.nextDouble();
                
                temp = Math.sqrt(k*0.1D);
                pH = -1*(Math.log10(temp));
                System.out.println("pH at 0.1M = " + df.format(pH));
                temp = Math.sqrt(k*0.01D);
                pH = -1*(Math.log10(temp));
                System.out.println("pH at 0.01M = " + df.format(pH));
                temp = Math.sqrt(k*0.001D);
                pH = -1*(Math.log10(temp));
                System.out.println("pH at 0.001M = " + df.format(pH));
                
            }
            if(choice.charAt(0) == 'b') {
                System.out.println("Enter Kb of substance (in the form #.##e#): ");
                k = sc.nextDouble();
                
                temp = Math.sqrt(k*0.1D);
                pOH = -1*(Math.log10(temp));
                pH = 14.00D - pOH;
                System.out.println("pH at 0.1M = " + df.format(pH));
                temp = Math.sqrt(k*0.01D);
                pOH = -1*(Math.log10(temp));
                pH = 14.00D - pOH;
                System.out.println("pH at 0.01M = " + df.format(pH));
                temp = Math.sqrt(k*0.001D);
                pOH = -1*(Math.log10(temp));
                pH = 14.00D - pOH;
                System.out.println("pH at 0.001M = " + df.format(pH));
            }
            else if(choice.charAt(0) != 'a' && choice.charAt(0) != 'b') {
                System.out.println("Invalid choice");
            }
            sc.nextLine();
            System.out.println("Enter another Ka/Kb? (yes/no)");
            again = sc.nextLine();
        }
    }
}


