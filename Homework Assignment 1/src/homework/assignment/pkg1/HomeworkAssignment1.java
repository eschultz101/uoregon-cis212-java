package homework.assignment.pkg1;

public class HomeworkAssignment1 {
    public static void main(String[] args) {
        HomeworkAssignment1.problemA(args);
        HomeworkAssignment1.problemB(args);
        HomeworkAssignment1.problemC(args);
        
    }
    public static void problemA(String[] args) {
        System.out.printf("CIS 35A Student Learning Outcomes\n" + 
                            "Outcome 1:\n" + 
                            "   Read, analyze and explain intermediate level Java\n" + 
                            "   programs.\n" + 
                            "Outcome 2:\n" +
                            "   Design solutions for intermediate level problems using\n" +
                            "   appropriate design methodology incorporating object-\n" +
                            "   oriented intermediate programming constructs.\n"  +
                            "Outcome 3:\n"  +
                            "   Create algorithms, code, document, debug, and test\n" + 
                            "   intermediate level Java programs.\n");
    }
    
    public static void problemB(String[] args) {
        System.out.printf("\nProblem B:");
        for (int var = 0x468A, i = 0; i < 3; i++) {
            var = var >> 4;
            System.out.printf("\n%X", var);
        }
        System.out.printf("\n\n");
    }
    
    public static void problemC(String[] args) {
        int divisor, sum = 0;
        System.out.printf("Perfect numbers between 2 and 500:\n");
        for (int i = 2; i <= 500; i++) {
            for (int j = 1; j <= i/2; j++) {
                if(i % j == 0) {
                    divisor = j;
                    sum += divisor;
                }
            }
            if (sum == i) {
                System.out.printf("%d\n", i);
            }
            sum = 0;
        }
    }
}
