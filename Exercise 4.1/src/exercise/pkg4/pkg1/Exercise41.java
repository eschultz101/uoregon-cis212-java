package exercise.pkg4.pkg1;

public class Exercise41 {
    public static void main(String[] args) {
        
        float[] array;
        int numElems = 8;
        float value = 10.0f;
        
        array = allocator(numElems, value);
        
        bPrint(array);
        
    }
    
    public static float[] allocator(int numElems, float value) {
        
        float[] array = new float[numElems];
        
        for (int i = 0; i < array.length; i++) {
            array[i] = value*i;
        }
        
        return array;
    }
    
    public static void bPrint(float[] array) {
        
        for(int i = array.length - 1; i >= 0; i--) {
            System.out.println(array[i]);
        }
        
    }
}
