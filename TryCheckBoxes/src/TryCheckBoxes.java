
import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

public class TryCheckBoxes extends JApplet 
                        implements ActionListener  {
        JLabel outputLabel;
	private JCheckBox chBoxA = new JCheckBox("A");
	private JCheckBox chBoxB = new JCheckBox("B");
	private JCheckBox chBoxC = new JCheckBox("C");
        private JCheckBox chBoxD = new JCheckBox("D");

  	public void init(){
   		setLayout(new FlowLayout()); 
		// put all components into JApplet
                chBoxA.addActionListener(this);
		add(chBoxA);
                chBoxB.addActionListener(this);
		add(chBoxB);
                chBoxC.addActionListener(this);
		add(chBoxC);
                chBoxD.addActionListener(this);
                add(chBoxD);
                outputLabel = new JLabel();
                add(outputLabel);
                
	 } // end init
        
        public void actionPerformed(ActionEvent e) {
            Object source = e.getSource();
                if( source == (Object) chBoxA )
                outputLabel.setText("Box A has been selected");
                else if( source == (Object) chBoxB )
                outputLabel.setText("Box B has been selected");
                else if( source == (Object) chBoxC )
                outputLabel.setText("Box C has been selected");
                else if( source == (Object) chBoxD )
                outputLabel.setText("Box D has been selected");
        } // end actionPerformed

} // end class TryCheckBoxes
