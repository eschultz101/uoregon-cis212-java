import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;


public class ConversionFrame extends JFrame{
	
	private static final long serialVersionUID = 7761897038230393603L;
	private JRadioButton fahr2Cels;
	private JRadioButton cels2Fahr;
	private ButtonGroup radioGroup;
	private JTextField textField;
	private JLabel tempLabel;
	private JButton convertButton;
	
	//Constuctor adds components into the JFrame
	public ConversionFrame() {
		//The text in the window title bar
		super("Temperature Conversion");
		
		//Set the components in a left-to-right order (centered by default).
		//Default for JFrame is BorderLayout
		setLayout(new FlowLayout());
		
		//Set the initial frame background to green
		getContentPane().setBackground(Color.GREEN);
		
		//Create the radio buttons
		fahr2Cels = new JRadioButton("Fahrenheit -> Celsius", true);
		cels2Fahr = new JRadioButton("Celsius -> Fahrenheit", false);
		
		//Add the radio buttons into the JFrame
		add(fahr2Cels);
		add(cels2Fahr);
		
		//Create logical relationships between the buttons, such
		//that only 1 button can be selected at a time
		radioGroup = new ButtonGroup();
		radioGroup.add(fahr2Cels);
		radioGroup.add(cels2Fahr);
		
		//Associate the item listener with the radio buttons
		fahr2Cels.addItemListener(new RadioButtonHandler());
		cels2Fahr.addItemListener(new RadioButtonHandler());
		
		//Create text box for entering a temperature, and add it
		//into the JFrame
		textField = new JTextField(10);
		add(textField);
		
		//Create the button that allows a user to convert the temperature,
		//and add the button into the JFrame.
		convertButton = new JButton("Convert");
		add(convertButton);
		
		//Associate the action listener with the button
		convertButton.addActionListener(new ConvertButtonHandler());
		
		//Create the label that displays the converted temperature
		//result.  This result is only displayed after the convert
		//button has been clicked.
		tempLabel = new JLabel();
		add(tempLabel);
		
	}
	
	private class ConvertButtonHandler implements ActionListener {
		
		public ConvertButtonHandler() {}
		
		public void actionPerformed(ActionEvent e) {
			String temperature = textField.getText();
			double tempDouble = Double.parseDouble(temperature);
			double conversion = 0;
			String symbol = "F";
			if(fahr2Cels.isSelected()) {
				conversion = (5.0 / 9) * (tempDouble - 32);
				symbol = "C";
			} else {
				conversion = 32 + (9.0 / 5) * tempDouble;
			}
			getContentPane().setBackground(Color.YELLOW);
			tempLabel.setText(Double.toString(conversion) + " " + symbol);
			//Clear the text field after each conversion
			textField.setText("");
		}
	}
	
	private class RadioButtonHandler implements ItemListener {
		
		public RadioButtonHandler() {}

		@Override
		public void itemStateChanged(ItemEvent e) {
			if(fahr2Cels.isSelected()) {
				getContentPane().setBackground(Color.GREEN);
			} else {
				getContentPane().setBackground(Color.ORANGE);
			}
		}
	}

}
