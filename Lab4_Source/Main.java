import javax.swing.JFrame;


public class Main {
	
	public static void main(String[] args) {
		
		ConversionFrame f = new ConversionFrame();
		
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(300, 200);
		f.setVisible(true);
		
	}
}
