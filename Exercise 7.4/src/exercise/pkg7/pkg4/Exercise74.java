package exercise.pkg7.pkg4;

import java.util.Scanner;
import java.util.InputMismatchException;

public class Exercise74 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Object data;
        
        boolean errorOccurred = false;
        do{
            errorOccurred = false;
            try{
                System.out.println("Enter integer for first node: ");
                data = sc.nextInt();
            }catch (InputMismatchException exp) {
                errorOccurred = true;
                System.out.println("Error: " + exp);
                data = sc.next();
            }
        } while (errorOccurred);

        Node node = new Node(data, null);
        data = node.getData();
        data = ((int) data) * 2;
        Node nNode = new Node(data, node.newNode());
        System.out.println(node.getData() + " " + nNode.getData());

    }
}

class Node { // Generic node of a linked list
    

    private Object data; // instead of C/C++ void*
    private Node next;

    public Node(Object o, Node n) {
        data = o;
        next = n;
    }
    
    public Object getData() {
        return data;
    }
    
    public void setData(Object newData) {
        data = newData;
    }
    
    public void setNext(Object newData) {
        next.data = newData;
    }
    
    public static Node newNode() {
        int i;
        Node nNode;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter integer for new node data: ");
        i = sc.nextInt();
        nNode = new Node(i, null);
        return nNode;
    }
} // end class Node

/*
run:
Enter integer for first node: 
3
Enter integer for new node data: 
2
3 6
BUILD SUCCESSFUL (total time: 2 seconds)
 */