import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;
import java.util.concurrent.ArrayBlockingQueue;



public class Main {
	
	public static void main(String[] args) {
		//ArrayList<Integer> arrayList = new ArrayList<Integer>();
		 List<TimeData> times = new ArrayList<TimeData>();
		 times.add(new TimeData(6, 24, 34));
		 TimeData temp1 = new TimeData(18, 14, 58);
		 times.add(temp1);
		 TimeData temp2 = new TimeData(6, 05, 34);
		 times.add(temp2);
		 times.add(new TimeData(12, 14, 58));
		 times.add(new TimeData(6, 24, 22));
		 //Duplicates
		 times.add(temp1);
		 times.add(temp2);
		 
		//Using Collections to sort the List
		Collections.sort(times, new TimeComparator());
			
		System.out.println("Sorted List of Times: " + times);
		 
		 //Count number of time occurences with a Map
		 Map<TimeData, Integer> counts = new HashMap<TimeData, Integer>();
		 for(TimeData t : times) {
			 if(counts.containsKey(t)) {
				 counts.put(t, counts.get(t) + 1);
			 } else {
				 counts.put(t, 1);
			 }
		 }
		 Iterator<Entry<TimeData, Integer>> elements = counts.entrySet().iterator(); 
		 while(elements.hasNext()) {
			 Entry<TimeData, Integer> e = elements.next();
			 System.out.println(e.getKey() + "\t:" + e.getValue());
		 }
		 
		 Set<TimeData> s = new HashSet<TimeData>(times);
		 System.out.println("Unsorted Set w/ no duplicates: " + s);
		 
		 Set<TimeData> s2 = new TreeSet<TimeData>(new TimeComparator());
		 s2.addAll(times);
		 System.out.println("Sorted Set w/ no duplicates: " + s2);
		
		 
		
		String a = "Hello";
		String b = "aba";
		String c = "kayak";
		String d = "world";
		
		char[] chars = c.toCharArray();
		Queue<Character> q = new ArrayBlockingQueue<Character>(chars.length);
		Stack<Character> stack1 = new Stack<Character>();
		for(int i = 0; i < chars.length; i++) {
			q.add(chars[i]);
			stack1.push(chars[i]);
		}
		
		boolean isPalindrome = true;
		for(int j = 0; j < chars.length; j++) {
			if(q.poll() != stack1.pop()) {
				isPalindrome = false;
				break;
			}
		}
		
		System.out.println(isPalindrome);
		
		
		
	}
	
	
	

}


