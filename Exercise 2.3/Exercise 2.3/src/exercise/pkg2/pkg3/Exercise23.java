package exercise.pkg2.pkg3;

public class Exercise23 {

    public static void main(String[] args) {
        int inum1 = 10, inum2, i;

        for (i = 0, inum2 = inum1; i < inum2; ++i, --inum2) {
            System.out.println("i = " + i);

        } // end for

        System.out.println("The last value of inum2 = " + inum2);
    }
}
