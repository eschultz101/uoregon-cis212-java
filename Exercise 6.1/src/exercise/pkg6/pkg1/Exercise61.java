package exercise.pkg6.pkg1;

import java.util.Scanner;

public class Exercise61 {
    public static void main(String[] args) {
        Customer cust = new Customer("No name", 123, 123);
        
        cust.setName("Schultz, Eric");
        
        System.out.println(cust.getName());
    }
}

class Customer {
    
    private StringBuilder lastName = new StringBuilder();
    private StringBuilder firstName = new StringBuilder();
    Scanner sc = new Scanner( System.in );
    
    private String name;
    private long acctNum;
    private SavingsAccount savAcct;
    
    public Customer(String tempStr, long tempInt, double tempDouble) {
        name = tempStr;
        acctNum = tempInt;
        savAcct = new SavingsAccount(tempDouble);
    }
    
    public String getName() {
        return (lastName + ", " + firstName);
    }
    
    public long getNum() {
        return acctNum;
    }
    
    public SavingsAccount getAcct() {
        return savAcct;
    }
    
    public boolean setName(String nm) {
        String delims = ", ";
        String[] splitName;
        String str = "a+b/(c-d)";
        if (nm != null && nm.length() > 0) {
            name = nm;
            splitName = nm.split(delims);
            lastName.append(splitName[0]);
            firstName.append(splitName[1]);
            
            String[] strArray = str.split("[+-/()]");
            
            System.out.println(strArray.length);
            System.out.println(strArray[0]);
            System.out.println(strArray[1]);
            //System.out.println(strArray[2]);
            System.out.println(strArray[3]);
            System.out.println(strArray[4]);
            
            return true;
        }
        return false;
    } 
}

class SavingsAccount {

    private static double annualIntRate; // default 0.
    private double balance; // default 0.

    public SavingsAccount(double bal) {

        if (bal > 0) {
            balance = bal;
        }
    // else leave as 0

    } // end constructor

    public SavingsAccount() {
    }//default const.

    public double getBalance() {
        return balance;
    }

    public boolean transaction(double amount) {

        if (balance + amount >= 0) {

            balance += amount;
            return true;

        }// end if
        return false;

    } // end transaction

    public double addMonthlyInterest() {

        double interest;

        interest = balance * annualIntRate / 12.;
// should be rounded to nearest 100th
        balance += interest;
        return interest;

    } // end addMonthlyInterest

    public static boolean modifyIntRate(double newRate) {

        if (newRate >= 0) {

            annualIntRate = newRate;
            return true;

        }// end if

        return false;

    } // end modifyIntRate

    public static double getAnnualIntRate() {

        return annualIntRate;

    } // end getAnnualIntRate
} // end SavingsAccount