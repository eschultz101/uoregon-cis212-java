package homework.assignment.pkg6;
import java.util.Scanner;

public class KenoGameDriver {
    public static Scanner scanner = new Scanner(System.in);
    public static int numSpots = 1;
    public static int bet = 1;
    
    public static void main(String[] args) {
        KenoGame game = new KenoGame();
        gameDriver(game);
    } // end main
    
    public static void gameDriver(KenoGame game) {
        String playAgain = "y";
        int num, b, index;
        
        while(playAgain.charAt(0) == 'y') {
            
            System.out.println("Choose a number of spots: ");
            num = scanner.nextInt();
            if(num > 0 && num < KenoGame.PAYOFF.length) {
                numSpots = num;
            }
            System.out.println("Place your bet: ");
            b = scanner.nextInt();
            if(b > 0) {
                bet = b;
            }
            game.newPlayer(numSpots, bet);
            
            game.drawNumbers();
            
            int i = 0, res;
            while(i < numSpots) {
                System.out.println("Enter an index: ");
                index = scanner.nextInt();
                res = game.setOneSpot(index);
                if(res == -1) {
                    System.out.println("Input out of range");
                }
                else if(res == 1) {
                    System.out.println("Input already chosen");
                }
                else if(res == 0) {
                    ++i;
                }
            } // end while i
            
            game.getResults();
            game.displayResults();
            
            scanner.nextLine();
            System.out.println("Play again? ");
            playAgain = scanner.nextLine();
        } // end while playAgain
    } // end gameDriver
} // end KenoGameDriver class
